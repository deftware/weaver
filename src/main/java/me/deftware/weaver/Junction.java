package me.deftware.weaver;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.Win32Exception;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.ptr.IntByReference;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.sun.jna.platform.win32.WinBase.INVALID_HANDLE_VALUE;

/**
 * Raw NTFS junction implementation
 * Based on https://github.com/Atry/scala-junction/blob/master/src/main/scala/com/dongxiguo/junction/Junction.scala
 *
 * @author Deftware
 */
public class Junction {

    public static final String PathPrefix = "\\??\\";

    public static WinNT.HANDLE open(Path file, int access, int share) {
        return Kernel32.INSTANCE.CreateFile(file.toString(),
                access,
                share,
                null,
                WinNT.OPEN_EXISTING,
                WinNT.FILE_FLAG_BACKUP_SEMANTICS | WinNT.FILE_FLAG_OPEN_REPARSE_POINT,
                null);
    }

    public static void link(Path link, Path target) throws IOException  {
        if (target.isAbsolute()) {
            if (!Files.exists(link)) {
                Files.createDirectories(link);
            } else if (!Files.isDirectory(link)) {
                throw new IOException("Link must be a directory, not a file");
            }
            mount(link, target);
        } else {
            throw new IOException("Target must be an absolute path");
        }
    }

    public static boolean isJunction(Path path) {
        if (!Files.exists(path)) {
            return false;
        }
        int attributes = Kernel32.INSTANCE.GetFileAttributes(path.toString());
        return FileAttributes.hasAttribute(attributes, FileAttributes.FILE_ATTRIBUTE_REPARSE_POINT);
    }

    private static void mount(Path link, Path targetPath) {
        WinNT.HANDLE handle = open(link, WinNT.GENERIC_READ | WinNT.GENERIC_WRITE, WinNT.FILE_SHARE_DELETE);
        if (handle == INVALID_HANDLE_VALUE) {
            throw new Win32Exception(Native.getLastError());
        }
        String target = targetPath.toString();

        int sizeOfPathBuffer = Character.SIZE / Byte.SIZE * (PathPrefix.length() + target.length() + 1 + target.length() + 1);
        int sizeOfMoutPointReparseBuffer = Byte.SIZE + sizeOfPathBuffer;
        int sizeOfReparseDataBuffer = Byte.SIZE + sizeOfMoutPointReparseBuffer;

        Memory reparseData = new Memory(sizeOfReparseDataBuffer);
        ByteBuffer buffer = reparseData.getByteBuffer(0, sizeOfReparseDataBuffer);

        short reserved = 0;
        short substituteNameOffset = 0;
        int substituteNameLength = Character.SIZE / Byte.SIZE * (PathPrefix.length() + target.length());
        int printNameOffset = substituteNameLength + Character.SIZE / Byte.SIZE;
        int printNameLength = Character.SIZE / Byte.SIZE * target.length();

        // File header
        buffer.putInt(FSTags.IO_REPARSE_TAG_MOUNT_POINT);
        buffer.putShort((short) sizeOfMoutPointReparseBuffer);
        buffer.putShort(reserved);
        buffer.putShort(substituteNameOffset);
        buffer.putShort((short) substituteNameLength);
        buffer.putShort((short) printNameOffset);
        buffer.putShort((short) printNameLength);

        // File data
        putString(buffer, PathPrefix);
        putString(buffer, target);
        buffer.putChar('\0');
        putString(buffer, target);
        buffer.putChar('\0');

        if (buffer.position() != buffer.capacity()) {
            throw new IllegalStateException("Expected " + buffer.capacity() + " bytes to be written, but only wrote " + buffer.position());
        }

        boolean writeStatus = Kernel32.INSTANCE.DeviceIoControl(
                handle,
                FSTags.FSCTL_SET_REPARSE_POINT,
                reparseData,
                sizeOfReparseDataBuffer,
                null,
                0,
                new IntByReference(),
                null
        );

        if (!writeStatus) {
            throw new Win32Exception(Native.getLastError());
        }

        Kernel32.INSTANCE.CloseHandle(handle);
    }

    private static void putString(ByteBuffer buffer, String s) {
        for (char c : s.toCharArray()) {
            buffer.putChar(c);
        }
    }

    /**
     * https://docs.microsoft.com/en-us/windows/win32/fileio/file-attribute-constants
     */
    public static class FileAttributes {
        public static final int FILE_ATTRIBUTE_REPARSE_POINT = 0x400;

        public static boolean hasAttribute(int file, int attr) {
            return (file & attr) != 0;
        }
    }

    /**
     * https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/c8e77b37-3909-4fe6-a4ea-2b9d423b1ee4
     */
    public static class FSTags {
        public static final int IO_REPARSE_TAG_MOUNT_POINT = 0xA0000003;
        public static final int FSCTL_SET_REPARSE_POINT = 0x000900a4;
    }

}
