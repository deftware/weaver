package me.deftware.weaver;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Version {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("release_target")
    private String target;

    @SerializedName("protocol_version")
    private int protocol;

    @SerializedName("stable")
    private boolean stable;

}
