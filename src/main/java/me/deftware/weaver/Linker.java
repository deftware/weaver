package me.deftware.weaver;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * @author Deftware
 */
public class Linker {

    private final Logger logger = new Logger("Linker");
    private final Path link, target;

    public Linker(Path link, Path target) {
        this.link = link;
        this.target = target;
    }

    public static boolean isSymlink(Path path) {
        if (SystemUtils.IS_OS_WINDOWS) {
            return Junction.isJunction(path);
        }
        return Files.isSymbolicLink(path);
    }

    public static void createSymlink(Path link, Path target) throws Exception {
        if (SystemUtils.IS_OS_WINDOWS) {
            Junction.link(link, target);
        } else {
            Files.createSymbolicLink(link, target);
        }
    }

    private void backup(Path backup) {
        if (Files.exists(backup)) {
            logger.warn("Backup directory already exists, skipping");
        } else {
            try (InputStream stream = Main.class.getResourceAsStream("/readme.txt")) {
                logger.info("Backing up previous link directory to %s", backup);
                Files.move(this.link, backup, StandardCopyOption.REPLACE_EXISTING);
                if (stream != null) {
                    Files.copy(stream, backup.resolve("README.txt"));
                }
            } catch (Exception ex) {
                logger.error("Unable to backup existing mods directory", ex);
            }
        }
    }

    public void run(Path backup) throws Exception {
        if (Files.exists(this.link)) {
            if (Files.isDirectory(this.link)) {
                logger.info("Found existing link directory");
                if (isSymlink(this.link)) {
                    Path target = this.link.toRealPath();
                    if (target.equals(this.target)) {
                        logger.info("Link already points to the correct target");
                        return;
                    }
                    logger.warn("Existing link points to the wrong target");
                } else if (Files.list(this.link).findFirst().isPresent()) {
                    this.backup(backup);
                }
            } else {
                logger.warn("Found file with link directory name, this shouldn't happen!");
            }
            // Check again since the link directory might
            // have been removed in previous stages
            if (Files.exists(this.link)) {
                logger.info("Deleting %s", this.link);
                FileUtils.forceDelete(this.link.toFile());
            }
        }
        logger.info("Creating symlink %s <<===>> %s", this.link, this.target);
        createSymlink(this.link, this.target);
    }

}
