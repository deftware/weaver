package me.deftware.weaver;

import com.google.gson.Gson;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import me.deftware.client.framework.util.path.LocationUtil;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.impl.launch.knot.Knot;

import java.io.*;
import java.nio.file.*;
import java.time.LocalDateTime;

public class Main {

    public static Version version;
    public static Path gameDir, libraries, versions, mods, emc, emcMods;

    private static Logger logger;

    public static void main(String[] args) {
        OptionParser parser = new OptionParser();
        parser.allowsUnrecognizedOptions();

        OptionSpec<String> versionArg = parser.accepts("version").withRequiredArg();
        OptionSpec<String> gameDirArg = parser.accepts("gameDir").withRequiredArg();
        OptionSet optionSet = parser.parse(args);

        gameDir = Paths.get(optionSet.valueOf(gameDirArg));
        libraries = gameDir.resolve("libraries");
        versions = gameDir.resolve("versions");
        mods = gameDir.resolve("mods");

        Logger.init(gameDir.resolve("weaver.log").toFile());
        logger = new Logger(Main.class.getSimpleName());
        logger.info("Weaver 1.0.0, current date and time is %s", LocalDateTime.now().toString());
        logger.info("Running on %s (%s)", System.getProperty("os.name"), System.getProperty("os.arch"));
        logger.info("Java %s", System.getProperty("java.version"));

        String versionName = optionSet.valueOf(versionArg);
        File versionJson = versions.resolve(versionName).resolve(versionName + ".json").toFile();
        logger.info("Found version %s", versionJson.getAbsolutePath());

        Gson gson = new Gson();
        try (InputStream stream = Main.class.getResourceAsStream("/version.json")) {
            if (stream == null) {
                throw new IOException("Unable to get resource stream");
            }
            try (Reader reader = new InputStreamReader(stream)) {
                version = gson.fromJson(reader, Version.class);
                logger.info("Detected Minecraft version %s", version.getTarget());
            }
        } catch (Exception ex) {
            throw new RuntimeException("Unable to get resource stream", ex);
        }

        emcMods = libraries.resolve("EMC").resolve(version.getTarget());
        try {
            LocationUtil location = LocationUtil.getEMC();
            File file = location.toFile();
            if (file == null) {
                throw new IOException("Location toFile returned null");
            }
            emc = Paths.get(file.getParent());
            logger.info("Found EMC path %s", emc.toString());
        } catch (Exception ex) {
            throw new RuntimeException("Unable to get location of EMC jar file", ex);
        }

        try {
            Linker linker = new Linker(mods, emc);
            linker.run(gameDir.resolve("mods-backup-" + version.getTarget()));
        } catch (Exception ex) {
            logger.error("An error occurred linking", ex);
        }

        // Run marketplace
        Marketplace.INSTANCE = new Marketplace(version);
        try {
            Marketplace.INSTANCE.init();
            Marketplace.INSTANCE.repair();
        } catch (Exception ex) {
            logger.error("Unable to run marketplace", ex);
        }

        logger.info("Starting game...");
        Logger.close();

        startGame(args);
    }

    private static void startGame(String[] args) {
        Knot.launch(args, EnvType.CLIENT);
    }

}
