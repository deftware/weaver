package me.deftware.weaver;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Download {

    @SerializedName("url")
    public String url;

    @SerializedName("loader")
    private String loader = "Fabric";

    @SerializedName("redirect")
    private Redirection redirection;

    @SerializedName("path")
    public FilePath path;

    @SerializedName("sha1")
    public String sha1;

    public boolean isCompatible() {
        return this.loader.equalsIgnoreCase("fabric");
    }

    public boolean isInstalled() {
        return Files.exists(this.path.getPath());
    }

    public void download() throws Exception {
        String url = this.url;
        if (this.redirection != null) {
            String contents = fetch(url);
            for (Instruction instruction : this.redirection.inst) {
                if (instruction.getType().equalsIgnoreCase("split")) {
                    contents = contents.split(instruction.getKeyword())[instruction.getIndex()];
                } else {
                    throw new RuntimeException("Unknown inst type " + instruction.getType());
                }
            }
            url = this.redirection.getUrl() + contents;
        }
        Path file = this.path.getPath();
        try (InputStream stream = new URL(url).openStream();
            BufferedInputStream buffer = new BufferedInputStream(stream)) {
            Files.copy(buffer, file);
        }
    }

    @Getter
    public static class FilePath {

        @SerializedName("type")
        private String type;

        @SerializedName("name")
        private String name;

        public Path getPath() {
            return Marketplace.INSTANCE.getPaths().get(this.type).resolve(
                    this.name.replace("%mc%", Marketplace.INSTANCE.getVersion().getTarget())
            );
        }

    }

    @Getter
    public static class Redirection {

        @SerializedName("url")
        private String url;

        @SerializedName("inst")
        private List<Instruction> inst;

    }

    @Getter
    public static class Instruction {

        @SerializedName("type")
        private String type;

        @SerializedName("keyword")
        private String keyword;

        @SerializedName("index")
        private int index;

    }

    public static String fetch(String url) throws Exception {
        try (InputStream stream = new URL(url).openStream();
            BufferedInputStream buffer = new BufferedInputStream(stream)) {
            return IOUtils.toString(buffer, StandardCharsets.UTF_8);
        }
    }

}
