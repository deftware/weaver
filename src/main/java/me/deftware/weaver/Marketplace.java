package me.deftware.weaver;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

public class Marketplace {

    public static Marketplace INSTANCE;

    private final Logger logger = new Logger("Marketplace");

    @Getter
    private final Map<String, Path> paths = new HashMap<>();

    @Getter
    private final Version version;

    private final Map<String, Module> modules = new HashMap<>();

    public Marketplace(Version version) {
        this.version = version;
        this.paths.put("fabric", Main.emc);
        this.paths.put("mods", Main.mods);
        this.paths.put("emc", Main.emcMods);
        this.paths.put("libraries", Main.libraries);
    }

    public void init() throws Exception {
        logger.info("Fetching marketplace index");
        URL url = new URL("https://gitlab.com/EMC-Framework/maven/-/raw/master/marketplace/index.json");
        try (InputStream stream = url.openStream(); Reader reader = new InputStreamReader(stream)) {
            JsonObject json = new Gson().fromJson(reader, JsonObject.class);
            Module[] modules = new Gson().fromJson(json.getAsJsonArray("mods"), Module[].class);
            for (Module m : modules) {
                m.run();
                if (m.isCompatible()) {
                    logger.info("Found compatible mod %s", m.id);
                    this.modules.put(m.id, m);
                }
            }
        }
    }

    public void repair() throws Exception {
        for (Module module : this.modules.values()) {
            if (!module.isInstalled()) {
                continue;
            }
            logger.info("Verifying installed mod %s", module.id);
            for (Download download : module.getCompatibleDownloads()) {
                File file = download.path.getPath().toFile();
                if (StringUtils.isEmpty(download.sha1)) {
                    logger.warn("Missing SHA1 for %s", file.getName());
                    continue;
                }
                // Verify checksum
                if (file.exists()) {
                    String checksum = this.SHA1(file);
                    if (checksum.equalsIgnoreCase(download.sha1)) {
                        logger.info("%s up to date", file.getName());
                        continue;
                    }
                    logger.warn("Invalid checksum for local file %s", file.getName());
                    logger.warn("Expected %s but found %s", download.sha1, checksum);
                    if (!file.delete()) {
                        logger.info("Unable to delete %s", file.getName());
                        continue;
                    }
                } else {
                    logger.warn("Missing file %s", file.getName());
                }
                logger.info("Downloading %s", download.url);
                download.download();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    private String SHA1(File file) throws Exception {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        try (InputStream stream = new FileInputStream(file);
             BufferedInputStream buffer = new BufferedInputStream(stream);
             DigestInputStream digestInputStream = new DigestInputStream(buffer, messageDigest)) {
            while (digestInputStream.read() != -1);
        }
        return new BigInteger(1, messageDigest.digest()).toString(16);
    }

}
