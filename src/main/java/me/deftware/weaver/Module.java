package me.deftware.weaver;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Module {

    @SerializedName("id")
    public String id;

    @SerializedName("downloads")
    private Map<String, List<Download>> downloadsMap;

    @SerializedName("depends")
    public List<String> depends;

    @Getter
    private final List<Download> compatibleDownloads = new ArrayList<>();

    public void run() {
        if (this.downloadsMap != null) {
            for (Map.Entry<String, List<Download>> entry : this.downloadsMap.entrySet()) {
                if (Main.version.getTarget().matches(entry.getKey())) {
                    for (Download download : entry.getValue()) {
                        if (download.isCompatible()) {
                            this.compatibleDownloads.add(download);
                        }
                    }
                    break;
                }
            }
        }
    }

    public boolean isInstalled() {
        return this.compatibleDownloads.stream().anyMatch(Download::isInstalled);
    }

    public boolean isCompatible() {
        return !this.compatibleDownloads.isEmpty();
    }

}
