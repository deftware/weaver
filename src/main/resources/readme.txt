This is a backup of your mods folder created after running EMC (Aristois) for the first time.

All of your previous mods were moved to this folder to ensure compatibility,
as EMC (Aristois) will link your mods folder to where EMC is located.

If you are certain that your mods will work with the
current Minecraft version, you can add them back to the mods folder.
